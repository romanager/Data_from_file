﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Student_get_info_from_file
{
    class Student : Person
    {
        public int Group { get; set; }
        private static StreamReader sr = null;

        public Student(string name, int? group) : base(name)
        {
            if (group != null)
            Group = (int)group;
            //Console.WriteLine("We are in the constructor of Student");
            Console.WriteLine("Old name: {0}, old group: {1}", Name, Group);
            this.GetInfo();
        }

        public override void GetInfo()
        {
            char ch = ' ';
            int indexOfSpace;
            try
            {
                string path = "student_info.txt";

                if (sr == null)
                {
                    sr = new StreamReader(path);
                }

                string line;
                line = sr.ReadLine();
                indexOfSpace = line.IndexOf(ch);
                Name = line.Substring(0, indexOfSpace);
                Group = Int32.Parse(line.Substring(indexOfSpace + 1));
                Console.WriteLine("New name of student: {0}", Name);
                Console.WriteLine("New group: {0}", Group);
                Console.WriteLine();
                if (!sr.EndOfStream)
                {
                    sr.Peek();
                }
                else
                {
                    Console.WriteLine("End of file");
                    sr.Close();
                    Console.WriteLine("File is closed");
                }
                
                //Console.WriteLine(line);

                /*while ((line = sr.ReadLine()) != null)
                    {
                        Console.WriteLine(line);
                 }*/
            }
            catch(Exception ex)
            {
                Console.WriteLine("An error has occured");
                Console.WriteLine("Message: " + ex.Message);
            }
        }
    }
}
