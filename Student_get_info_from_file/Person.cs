﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Student_get_info_from_file
{
    abstract class Person
    {
        public string Name { get; set; }
        
        public Person(string name)
        {
            Name = name;
            //Console.WriteLine("We are in the constructor of Person");
        }

        public abstract void GetInfo();
    }
}
