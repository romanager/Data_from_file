﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Student_get_info_from_file
{
    /* Created by Roman Matviienko */
    class Program
    {
        static void Main(string[] args)
        {
            List<Student> listStudents = new List<Student>();
            //Person st1 = new Student("first", 1);
            Student st1 = new Student("", null);
            //st1.GetInfo();
            listStudents.Add(st1);

            //Person st2 = new Student("second", 2);
            Student st2 = new Student("", 1);
            //st2.GetInfo();
            listStudents.Add(st2);

            Student st3 = new Student("s", 2);
            //st3.GetInfo();
            listStudents.Add(st3);

            Console.WriteLine("Count of students: {0}", listStudents.Count);

            Console.ReadKey();
        }
    }
}
